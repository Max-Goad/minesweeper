package com.maxgoad.minesweeper.module;

import com.maxgoad.minesweeper.settings.Setting;

public abstract class SettingsModule extends Module {

	public SettingsModule(ModuleRepository parent) {
		super(parent);
	}
	
	////////////////
	//Abstract Fns//
	////////////////
	public abstract int getSettingValue(Setting s);
	public abstract boolean setSettingValue(Setting s, int value);
}
