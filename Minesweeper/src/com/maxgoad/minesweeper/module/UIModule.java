package com.maxgoad.minesweeper.module;

import java.util.Map;

import com.maxgoad.minesweeper.gui.ScreenType;

public abstract class UIModule extends Module {

	public UIModule(ModuleRepository parent) {
		super(parent);
	}
	
	////////////////
	//Abstract Fns//
	////////////////
	public abstract void loadScreen(ScreenType s);
	public abstract void loadScreen(ScreenType s, Map<String, String> extraInfo);
	public abstract void forceLoadScreen(ScreenType s, Map<String, String> extraInfo);
	public abstract void updateScreen(ScreenType s, Map<String, String> extraInfo);
}
