package com.maxgoad.minesweeper.module;

public abstract class Module {
	
	protected ModuleRepository parent;
	
	////////////////
	//Constructors//
	////////////////
	protected Module(ModuleRepository parent) {
		this.parent = parent;
		
		if (this.parent == null) {
			throw new Error("A newly constructed Module must have a non-null parent!");
		}
	}
	
	////////////////
	//Abstract fns//
	////////////////
	public abstract void start();
	public abstract void stop();
}
