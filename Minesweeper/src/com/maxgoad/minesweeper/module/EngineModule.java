package com.maxgoad.minesweeper.module;

public abstract class EngineModule extends Module {
	
	public EngineModule(ModuleRepository parent) {
		super(parent);
	}
	
	////////////////
	//Abstract Fns//
	////////////////
	public abstract void startGame();
	public abstract void revealTile(int x, int y);
	public abstract void toggleTileFlag(int x, int y);
}
