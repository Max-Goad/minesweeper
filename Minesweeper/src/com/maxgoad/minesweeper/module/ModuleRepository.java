package com.maxgoad.minesweeper.module;


public class ModuleRepository {

	private EngineModule engineModule;
	private SettingsModule settingsModule;
	private UIModule uiModule;
	
	////////////////
	//Constructors//
	////////////////
	public ModuleRepository() {
	}
	
	////////////
	//Main fns//
	////////////
	public void startAllModules() {
		engineModule.start();
		settingsModule.start();
		uiModule.start();
	}
	
	public void stopAllModules() {
		engineModule.stop();
		settingsModule.stop();
		uiModule.stop();
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////
	public EngineModule getEngineModule() {
		if (engineModule != null)
			return engineModule;
		else
			throw new Error("EngineModule has not been set!");
	}
	
	public SettingsModule getSettingsModule() {
		if (settingsModule != null)
			return settingsModule;
		else
			throw new Error("SettingsModule has not been set!");
	}
	
	public UIModule getUiModule() {
		if (uiModule != null)
			return uiModule;
		else
			throw new Error("UIModule has not been set!");
	}
	
	public void setEngineModule(EngineModule engineModule) {
		this.engineModule = engineModule;
	}
	
	public void setSettingsModule(SettingsModule settingsModule) {
		this.settingsModule = settingsModule;
	}
	
	public void setUiModule(UIModule uiModule) {
		this.uiModule = uiModule;
	}
}
