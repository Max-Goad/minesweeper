package com.maxgoad.minesweeper.settings;

public enum Setting {
	GAMEDIFFICULTY,
	CUSTOM_X,
	CUSTOM_Y,
	CUSTOM_MINENUM;
}
