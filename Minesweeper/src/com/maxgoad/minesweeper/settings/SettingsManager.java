package com.maxgoad.minesweeper.settings;

import java.util.HashMap;

import com.maxgoad.minesweeper.module.*;

public class SettingsManager extends SettingsModule {
	
	private HashMap<Setting, Integer> settingsMap;
	private HashMap<Setting, Integer> settingsMinMap;
	private HashMap<Setting, Integer> settingsMaxMap;
	
	
	////////////////
	//Constructors//
	////////////////
	public SettingsManager(ModuleRepository parent) {
		super(parent);
		settingsMap = new HashMap<>();
		settingsMinMap = new HashMap<>();
		settingsMaxMap = new HashMap<>();
		
		start();
	}
	
	//////////////
	//Module fns//
	//////////////
	public void start() {
		populateDefaultSettings();
	}
	
	public void stop() {
		settingsMap.clear();
		settingsMinMap.clear();
		settingsMaxMap.clear();
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////
	private void populateDefaultSettings() {
		//TODO: Fill this out with real settings
		setSettingValue(Setting.GAMEDIFFICULTY, GameDifficulty.EXPERT.ordinal());

		settingsMinMap.put(Setting.CUSTOM_X, 3);
		settingsMaxMap.put(Setting.CUSTOM_X, 24);
		setSettingValue(Setting.CUSTOM_X, 10);

		settingsMinMap.put(Setting.CUSTOM_Y, 3);
		settingsMaxMap.put(Setting.CUSTOM_Y, 24);
		setSettingValue(Setting.CUSTOM_Y, 10);

		settingsMinMap.put(Setting.CUSTOM_MINENUM, 1);
		settingsMaxMap.put(Setting.CUSTOM_MINENUM, 100);
		setSettingValue(Setting.CUSTOM_MINENUM, 50);
		
	}
	
	public int getSettingValue(Setting s) {
		Integer settingValue = settingsMap.get(s);
		
		if (settingValue == null) {
			throw new Error("No setting of type " + s.toString() + " exists in settings map!");
		}
		else {
			return settingValue;
		}
	}
	
	public boolean setSettingValue(Setting s, int value) {
		if (settingsMinMap.containsKey(s) && settingsMaxMap.containsKey(s)) 
			if (value < settingsMinMap.get(s) || value > settingsMaxMap.get(s)) 
				return false;
		
		settingsMap.put(s, value);
		return true;
	}
}
