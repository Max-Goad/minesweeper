package com.maxgoad.minesweeper.settings;

public enum GameDifficulty {
	BEGINNER(9,9,10),
	INTERMEDIATE(16,16,40),
	EXPERT(16,30,99),
	RIDICULOUS(16,16,99),
	CUSTOM(-1,-1,-1);

	private int x;
	private int y;
	private int numMines;
	
	private GameDifficulty(int x, int y, int numMines) {
		this.x = x;
		this.y = y;
		this.numMines = numMines;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getNumMines() {
		return numMines;
	}
	
	//Static indexing code
	public static GameDifficulty getDifficultyFromInt(int x) {
		try {
			return GameDifficulty.values()[x];
		} catch (ArrayIndexOutOfBoundsException e) {
			return null;
		}
	}
	
}
