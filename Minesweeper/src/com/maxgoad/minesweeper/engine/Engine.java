/*
 * Class: 	MSEngine.java
 * Purpose:	Class containing the data representation of the Minesweeper game.
 * Creator:	Max Goad
 * Notes:	--
 * 
 */

package com.maxgoad.minesweeper.engine;

import java.util.HashMap;
import java.util.Map;

import com.maxgoad.minesweeper.gui.ScreenType;
import com.maxgoad.minesweeper.module.*;
import com.maxgoad.minesweeper.settings.GameDifficulty;
import com.maxgoad.minesweeper.settings.Setting;

public class Engine extends EngineModule {
	
	private Board board;
	
	////////////////
	//Constructors//
	////////////////
	public Engine(ModuleRepository parent) {
		super(parent);
	}
	
	//////////////
	//Module Fns//
	//////////////
	public void start() {
	}
	
	public void stop() {
		board = null;
	}
	
	///////////////////////
	//EngineInterface Fns//
	///////////////////////
	public void startGame() {
		SettingsModule settings = parent.getSettingsModule();
		GameDifficulty currentDifficulty = GameDifficulty.getDifficultyFromInt(settings.getSettingValue(Setting.GAMEDIFFICULTY));
		
		board = new Board();
		
		if (currentDifficulty.equals(GameDifficulty.CUSTOM)) {
			board.generateNewCustomBoard(settings.getSettingValue(Setting.CUSTOM_X),
										 settings.getSettingValue(Setting.CUSTOM_Y),
										 settings.getSettingValue(Setting.CUSTOM_MINENUM));
		}
		else {
			board.generateNewPresetBoard(currentDifficulty);
		}
		
		parent.getUiModule().forceLoadScreen(ScreenType.GAME, getBoardInfo());
	}
	
	public void revealTile(int x, int y) {
		boolean hitMine = board.revealTile(x, y);
		
		//TODO: Game over if hitMine
		
		parent.getUiModule().updateScreen(ScreenType.GAME, getBoardInfo());
	}
	
	public void toggleTileFlag(int x, int y) {
		board.toggleTileFlag(x, y);		
		parent.getUiModule().updateScreen(ScreenType.GAME, getBoardInfo());
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////
	private Map<String, String> getBoardInfo() {        
        Map<String, String> extraInfo = new HashMap<>();
        extraInfo.put("tileWidth", ""+board.getXLength());
        extraInfo.put("tileHeight", ""+board.getYLength());
        extraInfo.put("tileData", board.toString());
        
        return extraInfo;
	}
}
