package com.maxgoad.minesweeper.engine;

public class BoardTile {

	private int value;
	private boolean isUncovered;
	private boolean hasMine;
	private boolean hasFlag;
	
	////////////////
	//Constructors//
	////////////////
	public BoardTile() {
		value = 0;
		isUncovered = false;
		hasMine = false;
		hasFlag = false;
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		if (value >= 0 || value <= 8)
			this.value = value;
	}

	public boolean isUncovered() {
		return isUncovered;
	}

	public void uncover() {
		this.isUncovered = true;
	}

	public boolean hasMine() {
		return hasMine;
	}

	public void addMine() {
		this.hasMine = true;
	}
	
	public void removeMine() {
		this.hasMine = false;
	}

	public boolean hasFlag() {
		return hasFlag;
	}

	public void setHasFlag(boolean hasFlag) {
		this.hasFlag = hasFlag;
	}

	@Override
	public String toString() {		
		String returnString = String.valueOf(value);
		
		returnString += isUncovered ? 1 : 0;
		returnString += hasMine ? 1 : 0;
		returnString += hasFlag ? 1 : 0;
		
		return returnString;
	}
	
}
