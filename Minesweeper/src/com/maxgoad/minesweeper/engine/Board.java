package com.maxgoad.minesweeper.engine;

import java.util.Random;

import com.maxgoad.minesweeper.settings.GameDifficulty;

public class Board {

	private int xLength;
	private int yLength;
	
	private BoardTile[][] boardTiles;
	
	////////////////
	//Constructors//
	////////////////
	public Board() {
		
	}
	
	//////////////////
	//Generation fns//
	//////////////////
	public void generateNewPresetBoard(GameDifficulty newDifficulty) {
		if (newDifficulty == GameDifficulty.CUSTOM)
			return;
		
		xLength = newDifficulty.getX();
		yLength = newDifficulty.getY();
		
		generateBlankBoard();
		
		distributeMines(newDifficulty.getNumMines());
		
		assignTileValues();
		
	}
	
	public void generateNewCustomBoard(int newX, int newY, int numMines) {
		xLength = newX;
		yLength = newY;
		
		generateBlankBoard();

		distributeMines(numMines);
		
		assignTileValues();
		
	}
	
	public void generateBlankBoard() {
		boardTiles = new BoardTile[xLength][yLength];
		
		for (int x = 0; x < xLength; x++)
			for (int y = 0; y < yLength; y++)
				boardTiles[x][y] = new BoardTile();
	}
	
	private void distributeMines(int numMines) {
		Random rng = new Random();
		
		while (numMines > 0) {
			int targetX = rng.nextInt(xLength);
			int targetY = rng.nextInt(yLength);
			BoardTile targetTile = boardTiles[targetX][targetY];
			
			if (targetTile.hasMine()) {
				continue;
			}
			else {
				targetTile.addMine();
				numMines--;
			}
		}
	}
	
	private void assignTileValues() {
		for (int x = 0; x < xLength; x++) {
			for (int y = 0; y < yLength; y++) {
				BoardTile targetTile = boardTiles[x][y];
				
				if (targetTile.hasMine())
					continue;
				else
					targetTile.setValue(calculateTileValue(x,y));
			}
		}
	}
	
	private int calculateTileValue(int tileX, int tileY) {
		
		int numSurroundingMines = 0;
		
		for (int relativeX = -1; relativeX <= 1; relativeX++) {
			for (int relativeY = -1; relativeY <= 1; relativeY++) {
				if ((relativeX == 0 && relativeY == 0))
					continue;
				else if (tileX + relativeX >= xLength || tileX + relativeX < 0) 
					continue;
				else if (tileY + relativeY >= yLength || tileY + relativeY < 0)
					continue;
				
				if (boardTiles[tileX + relativeX][tileY + relativeY].hasMine()) {
					numSurroundingMines++;
				}
			}
		}
		
		return numSurroundingMines;
	}
	
	/////////////
	//Other fns//
	/////////////
	public boolean revealTile(int tileX, int tileY) {
		revealSurroundings(tileX, tileY);
		
		return boardTiles[tileX][tileY].hasMine();
	}
	
	private void revealSurroundings(int x, int y) {
		BoardTile hitTile = boardTiles[x][y];
		if (hitTile.isUncovered() || hitTile.hasFlag()) {
			return;
		}
		else {
			hitTile.uncover(); 
		}
		
		if (hitTile.getValue() == 0 && !hitTile.hasMine()) {
			
			//Left
			if (x - 1 >= 0) 
				revealSurroundings(x - 1, y);
			//Up
			if (y - 1 >= 0)
				revealSurroundings(x, y - 1);
			//Right
			if (x + 1 < xLength)
				revealSurroundings(x + 1, y);
			//Down
			if (y + 1 < yLength)
				revealSurroundings(x, y + 1);
			
			//Top L
			if (y - 1 >= 0 && x - 1 >= 0) 
				revealSurroundings(x - 1, y - 1);
			//Top R
			if (y - 1 >= 0 && x + 1 < xLength)
				revealSurroundings(x + 1, y - 1);
			//Bot L
			if (y + 1 < yLength && x - 1 >= 0) 
				revealSurroundings(x - 1, y + 1);
			//Bot R
			if (y + 1 < yLength && x + 1 < xLength)
				revealSurroundings(x + 1, y + 1);
		}
	}
	
	public void toggleTileFlag(int tileX, int tileY) {
		BoardTile targetTile = boardTiles[tileX][tileY];
		
		if (targetTile.isUncovered()) {
			return;
		}
		
		targetTile.setHasFlag(!targetTile.hasFlag());
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////
	@Override
	public String toString() {
		String returnString = "";
		
		//For each board tile
		for (int y = 0; y < yLength; y++) {
			for (int x = 0; x < xLength; x++) {
				returnString += boardTiles[x][y].toString() + "-";
			}
		}
		
		//trim final character
		return returnString.substring(0, returnString.length()-1);
	}
	
	public int getXLength() {
		return xLength;
	}
	
	public int getYLength() {
		return yLength;
	}
}
