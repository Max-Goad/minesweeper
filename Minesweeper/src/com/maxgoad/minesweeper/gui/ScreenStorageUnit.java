/*
 * Class: 	ScreenStorageUnit.java
 * Purpose:	
 * Creator:	Max Goad
 * Notes:	
 */

package com.maxgoad.minesweeper.gui;

import java.awt.Component;
import java.util.*;

public class ScreenStorageUnit {
	
	private ArrayList<Component> compList;
	private HashMap<SSUCompType, Component> compMap;
	
	////////////////
	//Constructors//
	////////////////
	public ScreenStorageUnit() {
		compList = new ArrayList<>();
		compMap = new HashMap<>();
	}

	////////////
	//Main fns//
	////////////
	public void addComponent(SSUCompType type, Component comp) {
		compList.add(comp);
		compMap.put(type, comp);
	}

	///////////////////
	//Getters/Setters//
	///////////////////	
	public ArrayList<Component> getAllComponents() {
		return compList;
	}
	
	public Component getComponent(SSUCompType type) {
		return compMap.get(type);
	}
}
