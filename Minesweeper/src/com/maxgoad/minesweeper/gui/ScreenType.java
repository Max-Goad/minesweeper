package com.maxgoad.minesweeper.gui;

public enum ScreenType {
	BLANK,
	GAME,
	MAIN_MENU,
	OPTIONS,
	TEST;
}
