package com.maxgoad.minesweeper.gui.builder;

import java.awt.*;

import javax.swing.*;

import com.maxgoad.minesweeper.gui.SSUCompType;

public class BlankScreenBuilder extends ScreenBuilder<BlankScreenBuilder> {

	public BlankScreenBuilder(JFrame frame) {
		super(frame);
	}
	
	public BlankScreenBuilder buildBasicScreen() {
		//Set some of basic variables to be used during the build process
		Color backgroundColour = BACKGROUND_DEFAULT_COLOUR;
 
        //Create background label
        JLabel backgroundLabel = new JLabel();
        backgroundLabel.setOpaque(true);
        backgroundLabel.setBackground(backgroundColour);
        backgroundLabel.setPreferredSize(new Dimension(frame.getWidth(), frame.getHeight() - MENU_BAR_DEFAULT_HEIGHT));
        
        ssu.addComponent(SSUCompType.GENERIC_BACKGROUND, backgroundLabel);
        
        return this;
	}
	
	
}
