package com.maxgoad.minesweeper.gui.builder;

import java.awt.*;

import javax.swing.*;

import com.maxgoad.minesweeper.gui.SSUCompType;
import com.maxgoad.minesweeper.gui.event.OptionsDifficultyEavesdropper;
import com.maxgoad.minesweeper.settings.GameDifficulty;

public class OptionsScreenBuilder extends ScreenBuilder<OptionsScreenBuilder> {

	public OptionsScreenBuilder(JFrame frame) {
		super(frame);
	}
	
	public OptionsScreenBuilder buildBasicScreen() {
		//Set some of basic variables to be used during the build process
		Color backgroundColour = new Color(140, 230, 185);
 
        //Create background label
        JLabel backgroundLabel = new JLabel();
        backgroundLabel.setOpaque(true);
        backgroundLabel.setBackground(backgroundColour);
        backgroundLabel.setPreferredSize(new Dimension(frame.getWidth(), frame.getHeight() - MENU_BAR_DEFAULT_HEIGHT));
        
        ssu.addComponent(SSUCompType.GENERIC_BACKGROUND, backgroundLabel);
        
        return this;
	}
	
	public OptionsScreenBuilder buildOptions(OptionsDifficultyEavesdropper eavesdropper) {
		
		//Create panel and constraints
		JPanel optionsPanel = new JPanel();
		optionsPanel.setLayout(new GridBagLayout());
		optionsPanel.setBorder(BorderFactory.createEmptyBorder(frame.getHeight()/10, frame.getWidth()/10, frame.getHeight()/10, frame.getWidth()/10));
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		
		//Difficulty ComboBox
		JComboBox<String> difficultyList = new JComboBox<>();
		for (GameDifficulty g : GameDifficulty.values())
			difficultyList.addItem(g.toString());
		difficultyList.setSelectedIndex(0);
		eavesdropper.addDifficultySelection(difficultyList);	//Apply eavesdropper
		constraints.gridx = 0;									//Apply constraints
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		constraints.weightx = 0.1;
		constraints.weighty = 0.5;
		constraints.insets = new Insets(5,5,5,5);
		optionsPanel.add(difficultyList, constraints);		//Add to panel
		
		//Custom Difficulty Options
		JLabel heightLabel = new JLabel("Height (3-24):");
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.gridwidth = 1;
		constraints.weightx = 0.1;
		constraints.weighty = 0.5;
		optionsPanel.add(heightLabel, constraints);
		
		JTextField heightTextField = new JTextField("");
		heightTextField.setEnabled(false);					//Start disabled
		eavesdropper.addListener(heightTextField); 			//Add to eavesdropper
		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.gridwidth = 1;
		constraints.weightx = 0.4;
		constraints.weighty = 0.5;
		optionsPanel.add(heightTextField, constraints);

		JLabel widthLabel = new JLabel("Width (3-24):");
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.gridwidth = 1;
		constraints.weightx = 0.1;
		constraints.weighty = 0.5;
		optionsPanel.add(widthLabel, constraints);
		
		JTextField widthTextField = new JTextField("");
		widthTextField.setEnabled(false);					//Start disabled
		eavesdropper.addListener(widthTextField); 			//Add to eavesdropper
		constraints.gridx = 1;
		constraints.gridy = 2;
		constraints.gridwidth = 1;
		constraints.weightx = 0.4;
		constraints.weighty = 0.5;
		optionsPanel.add(widthTextField, constraints);

		JLabel minesLabel = new JLabel("Mines (1-100):");
		constraints.gridx = 0;
		constraints.gridy = 3;
		constraints.gridwidth = 1;
		constraints.weightx = 0.1;
		constraints.weighty = 0.5;
		optionsPanel.add(minesLabel, constraints);
		
		JTextField minesTextField = new JTextField("");
		minesTextField.setEnabled(false);					//Start disabled
		eavesdropper.addListener(minesTextField); 			//Add to eavesdropper
		constraints.gridx = 1;
		constraints.gridy = 3;
		constraints.gridwidth = 1;
		constraints.weightx = 0.4;
		constraints.weighty = 0.5;
		optionsPanel.add(minesTextField, constraints);
		
		//Save Button
		JButton saveButton = new JButton("Save");
		eavesdropper.applySaveED(saveButton);
		constraints.gridx = 0;
		constraints.gridy = 4;
		constraints.gridwidth = 2;
		constraints.weightx = 0.1;
		constraints.weighty = 0.1;
		optionsPanel.add(saveButton, constraints);
		
		ssu.addComponent(SSUCompType.OPTIONS_PANEL, optionsPanel);
		return this;
	}
	
	
}
