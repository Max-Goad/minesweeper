package com.maxgoad.minesweeper.gui.builder;

import java.awt.*;

import javax.swing.*;

import com.maxgoad.minesweeper.gui.SSUCompType;
import com.maxgoad.minesweeper.gui.event.GameButtonEavesdropper;

public class GameScreenBuilder extends ScreenBuilder<GameScreenBuilder> {

	public GameScreenBuilder(JFrame frame) {
		super(frame);
	}
	
	public GameScreenBuilder buildBasicScreen() {
		//Set some of basic variables to be used during the build process
		//TODO: Pick a less ugly colour
		Color backgroundColour = new Color(102, 213, 248);
        
        //Create background label
        JLabel backgroundLabel = new JLabel();
        backgroundLabel.setLayout(new BorderLayout());
        backgroundLabel.setOpaque(true);
        backgroundLabel.setBackground(backgroundColour);
        backgroundLabel.setPreferredSize(new Dimension(frame.getWidth(), frame.getHeight() - MENU_BAR_DEFAULT_HEIGHT));
		
		ssu.addComponent(SSUCompType.GENERIC_BACKGROUND, backgroundLabel);
        
        return this;
	}
	
	public GameScreenBuilder buildGameBoard(int width, int height, GameButtonEavesdropper eavesdropper) {
		int bWidth = frame.getWidth()/(width+2);
		int bHeight = frame.getHeight()/(height+2);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new GridLayout(height,width));
		buttonPanel.setOpaque(false);
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(bHeight, bWidth, bHeight, bWidth));
	
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				//Create new button
				JButton newButton = new JButton();
				newButton.setMargin(new Insets(0,0,0,0));
				
				//Add action listener
				newButton.setActionCommand(x + "-" + y);
				eavesdropper.applyEavesdroppers(newButton);
				
				buttonPanel.add(newButton);
			}
		}
		
		ssu.addComponent(SSUCompType.GAME_BUTTONS, buttonPanel);
		
		return this;
	}
}