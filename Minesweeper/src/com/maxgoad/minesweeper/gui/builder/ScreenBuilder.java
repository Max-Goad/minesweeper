package com.maxgoad.minesweeper.gui.builder;

import java.awt.*;

import javax.swing.*;

import com.maxgoad.minesweeper.gui.ScreenStorageUnit;

public abstract class ScreenBuilder<T> {

	protected static final int MENU_BAR_DEFAULT_HEIGHT = 20;
	protected static final Color BACKGROUND_DEFAULT_COLOUR = new Color(248, 213, 131);
	
	protected JFrame frame;
	protected ScreenStorageUnit ssu;
	
	//////////////////
	//Constuctor fns//
	//////////////////
	public ScreenBuilder(JFrame frame) {
		this.frame = frame;
		this.ssu = new ScreenStorageUnit();
	}
	
	////////////
	//Main fns//
	////////////
	public abstract T buildBasicScreen();
	
	public ScreenStorageUnit publishSSU() {
		return ssu;
	}
}
