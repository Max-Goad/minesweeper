/*
 * Class: 	ScreenBuilder.java
 * Purpose:	
 * Creator:	Max Goad
 * Notes:	
 */

package com.maxgoad.minesweeper.gui;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.*;

import javax.swing.*;

import com.maxgoad.minesweeper.gui.builder.*;
import com.maxgoad.minesweeper.gui.event.*;
import com.maxgoad.minesweeper.module.ModuleRepository;

public class ScreenLoader {

	private HashMap<ScreenType, ScreenStorageUnit> ssuMap;
	
	private static GameButtonEavesdropper buttonEavesdropper;
	private static OptionsDifficultyEavesdropper optionsEavesdropper;
	
	private static JMenuBar menuBar;
	
	////////////////
	//Constructors//
	////////////////
	public ScreenLoader(ModuleRepository modRepo) {
		ssuMap = new HashMap<>();
		buttonEavesdropper = new GameButtonEavesdropper(modRepo);
		optionsEavesdropper = new OptionsDifficultyEavesdropper(modRepo);
		
		menuBar = generateMenuBar(modRepo);
	}

	////////////
	//Main fns//
	////////////	
	public void loadScreen(ScreenType s, JFrame frame, Map<String, String> extra) {
		if (ssuMap.get(s) == null) {
			forceLoadScreen(s, frame, extra);
		}
		else {
			loadScreenStorageUnit(frame, ssuMap.get(s));
		}
	}
	
	public void forceLoadScreen(ScreenType s, JFrame frame, Map<String, String> extra) {
		
		System.out.println("FORCING SCREEN LOAD OF TYPE " + s);
		
		ScreenStorageUnit ssu = null;
		
		switch(s) {
			case BLANK:
				ssu = new BlankScreenBuilder(frame).buildBasicScreen().publishSSU();
				
				ssuMap.put(s, ssu);
				break;
			case GAME:
				
				//TODO: Error check this plz					
				int tileWidth = Integer.parseInt(extra.get("tileWidth"));
				int tileHeight = Integer.parseInt(extra.get("tileHeight"));
				
				ssu = new GameScreenBuilder(frame).buildBasicScreen().buildGameBoard(tileWidth, tileHeight, buttonEavesdropper).publishSSU();
				
				ssuMap.put(s, ssu);
				updateScreen(ScreenType.GAME, extra);
				break;
			case MAIN_MENU:
				ssu = new MainMenuScreenBuilder(frame).buildBasicScreen().publishSSU();
				
				ssuMap.put(s, ssu);
				break;
			case OPTIONS:
				ssu = new OptionsScreenBuilder(frame).buildBasicScreen().buildOptions(optionsEavesdropper).publishSSU();
				
				ssuMap.put(s, ssu);
				break;
			default:
				throw new Error("Incorrect screenType passed to loadScreen");
		}
		
		loadScreenStorageUnit(frame, ssu);
	}
	
	public void updateScreen(ScreenType s, Map<String, String> extra) {
		switch (s) {
		case GAME:
			updateGameScreen(s, extra);
			break;

		default:
			break;
		}
	}
	
	//////////////
	//Helper fns//
	//////////////
	private static JMenuBar generateMenuBar(ModuleRepository modRepo) {
		if (menuBar != null) {
			return menuBar;
		}
		
		JMenuBar menuBar = new JMenuBar();
        menuBar.setOpaque(true);
        menuBar.setBackground(Color.GRAY);
		
        MenuBarEavesdropper eavesdropper = new MenuBarEavesdropper(modRepo);
        
        ////////////
        //Game tab//
        ////////////
        JMenu gameMenu = new JMenu("Game");
        gameMenu.setMnemonic(KeyEvent.VK_G);
        gameMenu.setDisplayedMnemonicIndex(-1);
        
        //Options for Game tab
        JMenuItem newGameItem = new JMenuItem("New Game");
        newGameItem.getAccessibleContext().setAccessibleDescription("TBD");
        newGameItem.setActionCommand("newGame");
        eavesdropper.applyEavesdroppers(newGameItem);

        JMenuItem statsItem = new JMenuItem("Statistics");
        statsItem.getAccessibleContext().setAccessibleDescription("TBD");
        statsItem.setActionCommand("stats");
        eavesdropper.applyEavesdroppers(statsItem);

        JMenuItem optionsItem = new JMenuItem("Options");
        optionsItem.getAccessibleContext().setAccessibleDescription("TBD");
        optionsItem.setActionCommand("options");
        eavesdropper.applyEavesdroppers(optionsItem);

        JMenuItem appearanceItem = new JMenuItem("Appearance");
        appearanceItem.getAccessibleContext().setAccessibleDescription("TBD");
        appearanceItem.setActionCommand("appearance");
        eavesdropper.applyEavesdroppers(appearanceItem);

        JMenuItem exitItem = new JMenuItem("Exit");
        exitItem.getAccessibleContext().setAccessibleDescription("TBD");
        exitItem.setActionCommand("exit");
        eavesdropper.applyEavesdroppers(exitItem);
        
        //Add options to Game tab
        gameMenu.add(newGameItem);
        gameMenu.addSeparator();
        gameMenu.add(statsItem);
        gameMenu.add(optionsItem);
        gameMenu.add(appearanceItem);
        gameMenu.addSeparator();
        gameMenu.add(exitItem);
        
        ////////////
	    //Help tab//
	    ////////////
        JMenu helpMenu = new JMenu("Help");
        helpMenu.setMnemonic(KeyEvent.VK_H);
        helpMenu.setDisplayedMnemonicIndex(-1);
        
        //Options for Help tab
        JMenuItem viewHelpItem = new JMenuItem("View Help");
        viewHelpItem.getAccessibleContext().setAccessibleDescription("TBD");
        viewHelpItem.setActionCommand("help");
        eavesdropper.applyEavesdroppers(viewHelpItem);

        JMenuItem aboutItem = new JMenuItem("About");
        aboutItem.getAccessibleContext().setAccessibleDescription("TBD");
        aboutItem.setActionCommand("about");
        eavesdropper.applyEavesdroppers(aboutItem);

        JMenuItem websiteItem = new JMenuItem("Visit the Author's Website");
        websiteItem.getAccessibleContext().setAccessibleDescription("TBD");
        websiteItem.setActionCommand("source");
        eavesdropper.applyEavesdroppers(websiteItem);
        
        //Add options to Help tab
        helpMenu.add(viewHelpItem);
        helpMenu.addSeparator();
        helpMenu.add(aboutItem);
        helpMenu.addSeparator();
        helpMenu.add(websiteItem);
        
        
        ///Add menu components to menuBar
        menuBar.add(gameMenu);
        menuBar.add(helpMenu);
		
		return menuBar;
	}
	
	private void loadScreenStorageUnit(JFrame frame, ScreenStorageUnit ssu) {
		this.loadScreenStorageUnit(frame, ssu, true);
	}
	
	private void loadScreenStorageUnit(JFrame frame, ScreenStorageUnit ssu, boolean hasMenuBar) {
		frame.getContentPane().removeAll();
		
		if (menuBar != null)
			frame.setJMenuBar(menuBar);
		
		ArrayList<Component> tempComps = ssu.getAllComponents();
		for (Component c : tempComps) {
			frame.getContentPane().add(c);
			System.out.println("Adding component " + c.toString());
		}

		frame.repaint();
		frame.revalidate();
	}
	
	private void updateGameScreen(ScreenType s, Map<String, String> extra) {
		ScreenStorageUnit ssu = ssuMap.get(s);
		
		Container buttons = (Container)ssu.getComponent(SSUCompType.GAME_BUTTONS);
		
		int tileWidth = Integer.parseInt(extra.get("tileWidth"));
		int tileHeight = Integer.parseInt(extra.get("tileHeight"));
		String[] tileArray = extra.get("tileData").split("-");
		
		int x, y, z = 0;
		for (x = 0; x < tileWidth; x++) {
			for (y = 0; y < tileHeight; y++) {
				//Create new button
				JButton button = (JButton)buttons.getComponent(z);
				
				/*
				 * tileData[0] = value (0-9)
				 * tileData[1] = isUncovered (bool)
				 * tileData[2] = hasMine (bool)
				 * tileData[3] = hasFlag (bool)
				 * Note: split("(?!^)") is the same as split(""), but works in ALL versions of Java, not just 8+
				 */					
				String[] tileData = tileArray[z].split("(?!^)"); 
				
				//Change appearance based on tileData
				
				//If is uncovered
				if (Integer.parseInt(tileData[1]) == 1) {
					
					button.setEnabled(false);
					
					//If is a mine
					if (Integer.parseInt(tileData[2]) == 1) {
						button.setText("M");
					}
					else if (tileData[0].equals("0")) {
						button.setText(null);
					}
					else {
						button.setText(tileData[0]);
					}
				}
				//If is hidden
				else {
					
					button.setEnabled(true);
					
					//If has a flag
					if (Integer.parseInt(tileData[3]) == 1) {
						button.setText("F");
					}
					else {
						button.setText(null);
					}
				}
				
				z++;
			}
		}
	}
	
	
}
