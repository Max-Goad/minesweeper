package com.maxgoad.minesweeper.gui.event;

import java.awt.event.*;

import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;

import com.maxgoad.minesweeper.module.ModuleRepository;

public class GameButtonEavesdropper {

	private ModuleRepository modRepo;
	
	public GameButtonEavesdropper(ModuleRepository modRepo) {
		this.modRepo = modRepo;
	}
	
	public void applyEavesdroppers(AbstractButton source) {
		source.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				doActionEavesdropping(source);
			}
		});
		
		source.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (SwingUtilities.isRightMouseButton(e)) {
					String command = source.getActionCommand();
					String[] tilePos = command.split("-");
					
					if (tilePos.length != 2) {
			        	throw new Error("String split error!");
			        }
					
					int x = Integer.parseInt(tilePos[0]);
					int y = Integer.parseInt(tilePos[1]);
					
					modRepo.getEngineModule().toggleTileFlag(x, y);
				}
			}
		});
	}
	
	private void doActionEavesdropping(AbstractButton source) {
		String command = source.getActionCommand();
		String[] tilePos = command.split("-");
		
		if (tilePos.length != 2) {
        	throw new Error("String split error!");
        }
		
		int x = Integer.parseInt(tilePos[0]);
		int y = Integer.parseInt(tilePos[1]);
		
		System.out.println("Tile (" + x + "," + y + ") revealed!");
		
		modRepo.getEngineModule().revealTile(x, y);
	}
}
