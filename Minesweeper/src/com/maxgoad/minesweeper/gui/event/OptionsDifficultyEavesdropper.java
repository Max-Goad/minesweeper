package com.maxgoad.minesweeper.gui.event;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import com.maxgoad.minesweeper.module.ModuleRepository;
import com.maxgoad.minesweeper.module.SettingsModule;
import com.maxgoad.minesweeper.settings.GameDifficulty;
import com.maxgoad.minesweeper.settings.Setting;

public class OptionsDifficultyEavesdropper {

	private ModuleRepository modRepo;
	
	private JComboBox<String> difficultySelection;
	private ArrayList<JTextField> listeners;
	
	public OptionsDifficultyEavesdropper(ModuleRepository modRepo) {
		this.modRepo = modRepo;
		
		difficultySelection = null;
		listeners = new ArrayList<JTextField>();
	}
	
	public void applySaveED(JButton source) {
		source.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				doSaveEavesdropping();
			}
		});
	}
	
	public void addDifficultySelection(JComboBox<String> source) {
		difficultySelection = source;
		difficultySelection.setSelectedIndex(modRepo.getSettingsModule().getSettingValue(Setting.GAMEDIFFICULTY));
		
		difficultySelection.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				doSelectionEavesdropping();
			}
		});
	}
	
	public void addListener(JTextField relatedComponent) {
		listeners.add(relatedComponent);
	}
	
	private void doSelectionEavesdropping() {
		GameDifficulty previousDifficulty = GameDifficulty.getDifficultyFromInt(modRepo.getSettingsModule().getSettingValue(Setting.GAMEDIFFICULTY));
		GameDifficulty selectedDifficulty = GameDifficulty.getDifficultyFromInt(difficultySelection.getSelectedIndex());
		
		if (previousDifficulty == GameDifficulty.CUSTOM || selectedDifficulty == GameDifficulty.CUSTOM) {
			for (JTextField c : listeners) {
				c.setEnabled(selectedDifficulty == GameDifficulty.CUSTOM);
			}
		}
	}
	
	private void doSaveEavesdropping() {
		GameDifficulty selectedDifficulty = GameDifficulty.getDifficultyFromInt(difficultySelection.getSelectedIndex());
		SettingsModule sMod = modRepo.getSettingsModule();
		
		sMod.setSettingValue(Setting.GAMEDIFFICULTY, selectedDifficulty.ordinal());
		if (selectedDifficulty == GameDifficulty.CUSTOM) {
			try {
				int customX = Integer.parseInt(listeners.get(0).getText());
				int customY = Integer.parseInt(listeners.get(1).getText());
				int customM = Integer.parseInt(listeners.get(2).getText());
				
				if (sMod.setSettingValue(Setting.CUSTOM_X, customX) == false
				 ||	sMod.setSettingValue(Setting.CUSTOM_Y, customY) == false
				 ||	sMod.setSettingValue(Setting.CUSTOM_MINENUM, customM) == false) {
					throw new NumberFormatException("Out of bounds information input into custom boxes!");
				}
			} catch (NumberFormatException e) {
				System.out.println("Error: " + e.getMessage());
				listeners.get(0).setText("");
				listeners.get(1).setText("");
				listeners.get(2).setText("");
			}
		}
		
	}
}
