package com.maxgoad.minesweeper.gui.event;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.AbstractButton;

import com.maxgoad.minesweeper.gui.ScreenType;
import com.maxgoad.minesweeper.module.ModuleRepository;

public class MenuBarEavesdropper {

	private ModuleRepository modRepo;
	
	public MenuBarEavesdropper(ModuleRepository modRepo) {
		this.modRepo = modRepo;
	}
	
	public void applyEavesdroppers(AbstractButton source) {
		source.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				doActionEavesdropping(source);
			}
		});
	}
	
	private void doActionEavesdropping(AbstractButton source) {
		String command = source.getActionCommand();
		
		switch(command) {
			//Game menu
			case "newGame":
				modRepo.getEngineModule().startGame();
				break;
			case "stats":
				break;
			case "options":
				modRepo.getUiModule().loadScreen(ScreenType.OPTIONS);
				break;
			case "appearance":
				break;
			case "exit":
				modRepo.stopAllModules();
				break;
				
			//Help menu
			case "help":
				break;
			case "about":
				break;
			case "source":
				openURL("http://www.maxgoad.com/");
				break;
				
			default:
				throw new Error("MenuBarListener encountered an unknown action command! " + command);
		}
	}
	
	private void openURL(String url) {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(new URL(url).toURI());
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}
}
