package com.maxgoad.minesweeper.gui;

public enum SSUCompType {
	//Generic
	GENERIC_BACKGROUND,
	//Game
	GAME_BUTTONS,
	//Options
	OPTIONS_PANEL
}
