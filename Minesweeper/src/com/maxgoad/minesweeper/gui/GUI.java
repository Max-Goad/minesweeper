/*
 * Class: 	MSUserInterface.java
 * Purpose:	Main GUI class for Minesweeper game 
 * Creator:	Max Goad
 * Notes:	--
 * 
 */

package com.maxgoad.minesweeper.gui;

import java.util.Map;

import javax.swing.*;

import com.maxgoad.minesweeper.module.*;

public class GUI extends UIModule {

	private static final int DEFAULT_FRAME_WIDTH = 700;
	private static final int DEFAULT_FRAME_HEIGHT = 720;
	
	private JFrame frame;
	private ScreenLoader screenLoader;
	
	////////////////
	//Constructors//
	////////////////
	public GUI(ModuleRepository parent) {
		super(parent);
		
		frame = new JFrame("Minesweeper");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);
		
		screenLoader = new ScreenLoader(parent);
	}

	//////////////
	//Module fns//
	//////////////
	public void start() {
		this.loadScreen(ScreenType.BLANK);
		
        frame.pack();
        frame.setVisible(true);
	}
	
	public void stop() {
		frame.dispose();
	}
	
	@Override
	public void loadScreen(ScreenType s) {
		this.loadScreen(s, null);
	}
	
	@Override
	public void loadScreen(ScreenType s, Map<String, String> extraInfo) {
		screenLoader.loadScreen(s, frame, extraInfo);
	}
	
	@Override
	public void forceLoadScreen(ScreenType s, java.util.Map<String,String> extraInfo) {
		screenLoader.forceLoadScreen(s, frame, extraInfo);
	}
	
	@Override
	public void updateScreen(ScreenType s, Map<String, String> extraInfo) {
		screenLoader.updateScreen(s, extraInfo);

		frame.repaint();
		frame.revalidate();
	}
	
	///////////////////
	//Getters/Setters//
	///////////////////
}