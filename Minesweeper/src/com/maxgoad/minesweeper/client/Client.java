/*
 * Class: 	MSClient.java
 * Purpose:	Main Client class for Minesweeper game 
 * Creator:	Max Goad
 * Notes:	--
 * 
 */

package com.maxgoad.minesweeper.client;

import com.maxgoad.minesweeper.engine.Engine;
import com.maxgoad.minesweeper.gui.GUI;
import com.maxgoad.minesweeper.module.ModuleRepository;
import com.maxgoad.minesweeper.settings.SettingsManager;

public class Client {
	
	private ModuleRepository moduleRepository;
	
	////////////////
	//Constructors//
	////////////////
	public Client() {
		moduleRepository = new ModuleRepository();
		
		Engine e = new Engine(moduleRepository);
		SettingsManager s = new SettingsManager(moduleRepository);
		GUI g = new GUI(moduleRepository);
		
		moduleRepository.setEngineModule(e);
		moduleRepository.setSettingsModule(s);
		moduleRepository.setUiModule(g);
	}
	
	/////////////////
	//Startable Fns//
	/////////////////
	public void start() {
		moduleRepository.startAllModules();
	}
	
	public void stop() {
		moduleRepository.stopAllModules();
	}	
}
